const { Server } = require("socket.io");
const shajs = require("sha.js");
const fetch = require('node-fetch');

const enableBackupDatabase = false;
const errorOut = false;
const devMode = false;

const admins = ["LukasExists", "L413", "Redstone-Network"];

fetch("https://google.com")
  .then(response => {
    console.log(response.headers.get("Content-Type"));
  });

if (errorOut) {
  throw new Error("Controlled error, errorOut = true");
}

const captchajs = require("./captcha.js");
const fs = require("fs");

const { Gitlab } = require("@gitbeaker/node");
const gitApi = new Gitlab({
  token: process.env.gittag,
});

const COLORS = [
  "#e21400", "#91580f", "#f8a700", "#f78b00", "#f44611", "#87ceeb", "#58dc00", "#287b00",
  "#a8f07a", "#4ae8c4", "#f28500", "#dc143c", "#3b88eb", "#3824aa", "#a700ff", "#d300e7",
  "#dfff00", "#d2b48c",
];

let apps = [];
let themes = [];

let data = null;

let bc_db = null;

gitApi.RepositoryFiles.showRaw("45210004", "data.json", { ref: "main" })
  .then((response) => {
    if (enableBackupDatabase) {
      console.log("Attempting to revert to backup...");
      fs.writeFile(".data/data.json", response, (err) => {
        if (err) {
          console.error("Failed to revert to backup of database");
        } else {
          console.log("Reverted to backup of database successfully, exiting...");
          process.exit(0);
        }
      });
    } else {
      bc_db = "denied B)";
    }
  })
  .catch((e) => {
    console.error(e.name + ": " + e.description);
  });

if (!enableBackupDatabase && !devMode) {
  fs.readFile(".data/data.json", "utf8", (err, rawdata) => {
    if (err) {
      console.error(err);
      return;
    }
    data = JSON.parse(rawdata);
    if (!data.themes) {
      data.themes = [];
    }
    console.log(data.accounts.length);
    let listOfApps = "LIST OF APPS:\n";
    for (let i = 0; i < data.apps.length; i++) {
      listOfApps +=
        "| " + data.apps[i].name + " (by: " + data.apps[i].uploader + ") |";
    }
    if (listOfApps !== "LIST OF APPS:\n") {
      console.log(listOfApps);
    } else {
      console.log("No apps found.");
    }
  });
}

const maintMode = false;

const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = new Server(server);
const port = process.env.PORT || 3000;

function generateToken(len) {
  const hex = "0123456789ABCDEF!?abcdef-";
  let output = "";
  for (let i = 0; i < len; ++i) {
    output += hex.charAt(Math.floor(Math.random() * hex.length));
  }
  if (output.endsWith("-")) {
    output = output.slice(0, output.length - 1) + "!";
  }
  return output;
}

function saveData() {
  var data2 = JSON.stringify(data);
  var e2;
  fs.writeFile(".data/data.json", data2, (err) => {
    e2 = err ? "failure" : "success";
    console.log("file write " + e2);
  });
  return e2;
}

function backupData() {
  var e2;
  var backupData = JSON.stringify(data);
  gitApi.Commits.create("45210004", "main", "Backup time!", [
    {
      action: "update",
      filePath: "data.json",
      content: backupData,
    },
  ]).catch((e) => {
    console.error(e.name + ": " + e.description);
  });
  return e2;
}

if (!devMode) {
  setInterval(saveData, 5000);

  setInterval(backupData, 15000);

  server.listen(port, function () {
    console.log("---CLOCKWORK APP STORE---\nServer listening at port %d", port);
  });

  function createCaptcha() {
    return captchajs.captcha();
  }

  // Routing
  app.use(express.static("public"));

  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/v2/index.html');
  });

  app.get('/dev-' + process.env.databaseReadPassword, (req, res, next) => {
    res.send(JSON.stringify(data));
  });

  io.on("connection", function (socket) {
    socket.loggedIn = false;
    socket.username = null;
    socket.apps = [];
    socket.themes = [];
    socket.captcha = createCaptcha();
    
    socket.on("sign up", function (cldata, callback) {
      var result = "server failure";
      var token = generateToken(128);
      var hashword = shajs("sha256").update(cldata.password).digest("hex");
      var user = data.accounts.find((x) => x.username === cldata.username);
      if (!user) { 
        result = (function() {
          if (cldata.password.length < 8) {
            return "password too short";
          }
          if (cldata.password.length < 3) {
            return "name too short";
          }
          if (cldata.password.length > 32) {
            return "name too long";
          }
          if (!/^[a-zA-Z0-9\-\_]+$/.test(cldata.username)) {
            return "name not valid";
          }
          if (cldata.captcha != socket.captcha.code) {
            console.log(socket.captcha.code);
            return "captcha failed"
          }
          user = {
            username: cldata.username,
            hashword: hashword,
            admin: admins.includes(socket.username),
            rank: "user",
            token: token,
          }
          data.accounts.push(user)
          return "success";
        })();
      } else {
        result = "user not found";
      }
      if (result == "success") {
        user.token = token;
        socket.apps = data.apps.filter((item) => item.uploader == cldata.username);
        socket.themes = data.themes.filter((item) => item.uploader == cldata.username);
        socket.loggedIn = true;
        socket.username = cldata.username
        callback({
          result: result,
          token: token,
          apps: socket.apps,
        });
      } else {
        callback({
          result: result,
        });
      }
    });

    socket.on("remove app", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          var app = data.apps.find((x) => x.url === cldata.url)
          if (app == undefined) {
            throw new Error("app doesnt exists");
          }
          if (app.uploader != socket.username) {
            throw new Error("app not owned by user");
          }
          data.apps.splice(data.apps.indexOf(app), 1);
          socket.apps.splice(socket.apps.indexOf(app), 1);
          callback({
            result: "success",
            apps: socket.apps,
          })
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("admin cmd", function (cldata, callback) {
      if (socket.loggedIn) {
        if (admins.includes(socket.username)) {
          var bD = cldata.text.match(/ban ([a-zA-Z0-9_\-]{3,24})( [^]+||)/);
          var ubD = cldata.text.match(/unban ([a-zA-Z0-9_\-]{3,24})( [^]+||)/);
          if (bD) {
            var usr = data.accounts.find((x) => x.username === bD[1]);
            if (usr || usr.rank == "user") {
              for (let i = 0; i < data.accounts.length;) {
                if (data.accounts[i].username == usr.username) { 
                  data.accounts[i].rank = "banned";
                  data.accounts[i].banReason = bD[2];
                }
                ++i;
              }
              callback({
                result: "success",
              })
            } else {
              callback({
                result: "nonexistent acc / incorrect rank",
              })
            }
          }
          
          if (bD) {
            var usr = data.accounts.find((x) => x.username === ubD[1]);
            if (usr || usr.rank == "banned") {
              for (let i = 0; i < data.accounts.length;) {
                if (data.accounts[i].username == usr.username) { 
                  data.accounts[i].rank = "user";
                  data.accounts[i].banReason = bD[2];
                }
                ++i;
              }
              callback({
                result: "success",
              })
            } else {
              callback({
                result: "nonexistent acc / incorrect rank",
              })
            }
          }
        } else {
          callback({
            result: "incorrect permissions",
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("remove theme", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          var app = data.themes.find((x) => x.url === cldata.url)
          if (app == undefined) {
            throw new Error("app doesnt exists");
          }
          if (app.uploader != socket.username) {
            throw new Error("app not owned by user");
          }
          data.themes.splice(data.themes.indexOf(app), 1);
          socket.themes.splice(socket.themes.indexOf(app), 1);
          callback({
            result: "success",
            themes: socket.themes,
          })
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("edit app", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          var app = data.apps.find((x) => x.url === cldata.url)
          if (app == undefined) {
            throw new Error("app doenst exists");
          }
          if (app.uploader != socket.username) {
            throw new Error("app not owned by user");
          }
          fetch(cldata.url)           //api for the get request
            .then(response => response.json())
            .then(zdata => {
              var myAppEntry = {
                name: zdata.name,
                description: zdata.desc,
                url: cldata.url,
                uploader: zdata.author,
                icon: zdata.icon,
              }
              if (zdata.author != socket.username) {
                throw new Error("'author' property does not match username");
              }
              data.apps = data.apps.map((x) => {
                if (x.url === cldata.url) {
                  return myAppEntry;
                } else {
                  return x;
                }
              })
              socket.apps = socket.apps.map((x) => {
                if (x.url === cldata.url) {
                  return myAppEntry;
                } else {
                  return x;
                }
              })
              callback({
                result: "success",
                data: myAppEntry,
                apps: socket.apps,
              })
            }
          );
          
          callback({
            result: "success",
            apps: socket.apps,
          })
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("edit theme", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          var app = data.themes.find((x) => x.url === cldata.url)
          if (app == undefined) {
            throw new Error("theme doesn't exist");
          }
          if (app.uploader != socket.username) {
            throw new Error("theme not owned by user");
          }
          fetch(cldata.url)           //api for the get request
            .then(response => response.text())
            .then(zdata => {
              var title = zdata.match(/^\/\*theme title: ([^\n]+)\*\/$/m);
              if (title) title = title[1];
              var desc = zdata.match(/^\/\*theme desc: ([^\n]+)\*\/$/m);
              if (desc) desc = desc[1];
              var author = zdata.match(/^\/\*theme author: ([^\n]+)\*\/$/m);
              if (author) author = author[1];
              var banner = zdata.match(/^\/\*theme banner: ([^\n]+)\*\/$/m);
              if (banner) banner = banner[1];
              
              var myAppEntry = {
                name: title,
                description: desc,
                url: cldata.url,
                uploader: author,
                banner: banner,
              }
              
              if (author != socket.username) {
                throw new Error("'author' property does not match username");
              }
              if (!title) {
                throw new Error("'title' property missing");
              }
            
              data.themes = data.themes.map((x) => {
                if (x.url === cldata.url) {
                  return myAppEntry;
                } else {
                  return x;
                }
              })
              socket.themes = socket.themes.map((x) => {
                if (x.url === cldata.url) {
                  return myAppEntry;
                } else {
                  return x;
                }
              })
              callback({
                result: "success",
                data: myAppEntry,
                themes: socket.themes,
              })
            }
          );
          
          callback({
            result: "success",
            apps: socket.apps,
          })
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("add app", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          if (data.apps.find((x) => x.url === cldata.url)) {
            throw new Error("app already exists");
          }
          var url = cldata.url;
          fetch(url)           //api for the get request
            .then(response => {
              if (!response.headers.get("Content-Type").startsWith("application/json")) {
                throw new Error("incorect content-type headers given");
              }
              response.json().then(zdata => {
                var myAppEntry = {
                  name: zdata.name,
                  description: zdata.desc,
                  url: url,
                  uploader: zdata.author,
                  icon: zdata.icon,
                }
                if (zdata.author != socket.username) {
                  throw new Error("'author' property does not match username");
                }
                data.apps.push(myAppEntry);
                socket.apps.push(myAppEntry);
                callback({
                  result: "success",
                  data: myAppEntry,
                  apps: socket.apps,
                })
              })
            }).catch((e) => {
              callback({
                result: "error",
                error: e.name + ": " + e.message,
              })
            })
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });
    
    socket.on("add theme", function (cldata, callback) {
      if (socket.loggedIn) {
        try {
          if (data.themes.find((x) => x.url === cldata.url)) {
            throw new Error("theme already exists");
          }
          var url = cldata.url;
          fetch(url)           //api for the get request
            .then(response => {
              if (!response.headers.get("Content-Type").startsWith("text/css")) {
                throw new Error("incorect content-type headers given");
              }
              response.text().then(zdata => {
                var title = zdata.match(/^\/\*theme title: ([^\n]+)\*\/$/m);
                if (title) title = title[1];
                var desc = zdata.match(/^\/\*theme desc: ([^\n]+)\*\/$/m);
                if (desc) desc = desc[1];
                var author = zdata.match(/^\/\*theme author: ([^\n]+)\*\/$/m);
                if (author) author = author[1];
                var banner = zdata.match(/^\/\*theme banner: ([^\n]+)\*\/$/m);
                if (banner) banner = banner[1];
              
                var myAppEntry = {
                  name: title,
                  description: desc,
                  url: url,
                  uploader: author,
                  banner: banner,
                }
                if (author != socket.username) {
                  throw new Error("'author' property does not match username");
                }
                if (!title) {
                  throw new Error("'title' property missing");
                }
                data.themes.push(myAppEntry);
                socket.themes.push(myAppEntry);
                callback({
                  result: "success",
                  data: myAppEntry,
                  themes: socket.themes,
                })
              })
            }).catch((e) => {
              callback({
                result: "error",
                error: e.name + ": " + e.message,
              })
            });
            
        } catch(e) {
          callback({
            result: "error",
            error: e.name + ": " + e.message,
          })
        }
      } else {
        callback({
          result: "not logged in",
        })
      }
    });

    socket.on("get data", function (cldata, callback) {
      callback({
        apps: data.apps.slice(
          30 * (cldata.page.apps - 1),
          30 * cldata.page.apps
        ),
        themes: data.themes.slice(
          30 * (cldata.page.themes - 1),
          30 * cldata.page.themes
        ),
        pages: {
          apps: Math.ceil(data.apps.length / 30),
          themes: Math.ceil(data.themes.length / 30),
        },
      });
    });

    socket.on("get captcha", function (cldata, callback) {
      socket.captcha = createCaptcha();
      callback({
        captcha: socket.captcha.image,
      });
    });
  });
} else {
  app.get('/test', (req, res, next) => {
    res.send('test');
  });
}