# CURRENT PLAN [LukasExists]
1. Add account system to v2

DONE

2. Add ability to upload apps

DONE

3. Add ability to reload app data

DONE

4. Add moderation tools

DONE

5. Add ability to upload themes

DONE

6. Add ability to edit and remove themes

DONE

7. Add search function

COMING SOON