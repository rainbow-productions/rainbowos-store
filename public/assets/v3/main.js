import io from "../socket.io/socket.io.js"
var socket = io()

if (window === window.parent) {
  //alert("it seems you aren't viewing the app store on clockwork! keep in mind you wont be able to install apps")
  document.body.style = "background-color: #000;";
}

$(".topbar")
  .html(`<input type='text' class='search' placeholder='Search for an app or theme'>
<button class="search-button icon-btn">
<img src="https://cdn.glitch.global/423a0a44-b287-44e4-a45f-9cabc958361c/magnifying-glass.png?v=1704896616726" alt="Search button">
</button>`);

$(".search-button").click((e) => {
  if ($(".search").val().length > 0) {
    document.location.href =
      "/v3/search?q=" + encodeURIComponent($(".search").val());
  }
});

$(document).on("keydown", "input", function (e) {
  if ((e.key == "Enter") && $(".search").is(":focus")) {
    $(".search-button").click()
  }
});
