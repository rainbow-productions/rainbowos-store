const socket = io();

var loggedIn = null;
var username = null;

var apps = [];
var themes = [];

/*
if ( window === window.parent ) {
  alert("it seems you aren't viewing the app store on clockwork! keep in mind you wont be able to install apps")
} 
*/

function openTab(tab) {
  document.getElementById("apps").style = "display: none;";
  document.getElementById("themes").style = "display: none;";
  document.getElementById("dev").style = "display: none;";
  document.getElementById(tab).style = "";
}

function installTheme(script) {
  window.parent.postMessage(["install theme",script], "*");
}

function installApp(script) {
  window.parent.postMessage(["install app",script], "*");
}

function loadAll() {
  // APPS
  var html = "";
  for (var i = 0; i < apps.length; i++) {
    html += `<div class="grid-item">
    <div class="grid-objects">
      <span style="font-size: 17px;"><b>`+apps[i].name+`</b> by `+apps[i].uploader+`</span><br>
      `+apps[i].description+`
    </div><br>
    <div class="grid-objects">
      <img class="grid-objects" src="`+apps[i].icon+`">
      <input type="button" onclick="installApp('`+apps[i].url+`');" value="Install" class="button1" style="height: 36px; vertical-align: top;">
    </div>
  </div>`;
  }
  var div = document.createElement("DIV");
  div.innerHTML = html;
  div.className = "grid-container";
  div.id = "apps";
  document.body.appendChild(div);
  document.getElementById("apps").innerHTML = html;
  
  // THEMES
  var html = "";
  for (var i = 0; i < themes.length; i++) {
    html += `<div class="grid-item">
    <div class="grid-objects">
      <span style="font-size: 17px;"><b>`+themes[i].name+`</b> by `+themes[i].uploader+`</span><br>
      `+themes[i].description+`
    </div><br>
    <div class="grid-objects"><img class="grid-objects" src="${themes[i].banner}" onerror="this.onerror=null;this.src='https://placeimg.com/200/300/animals';">
      <input type="button" onclick="installTheme('`+themes[i].url+`');" value="Install" class="button1" style="height: 36px; vertical-align: top;">
    </div>
  </div>`;
  }
  var div = document.createElement("DIV");
  div.innerHTML = html;
  div.className = "grid-container";
  div.id = "themes";
  document.body.appendChild(div);
  document.getElementById("themes").innerHTML = html;
  document.getElementById("themes").style = "display: none;";
  document.getElementById("loading").style = "display: none;";
}

// This part gets the list of apps!
socket.emit("get legacy data", false, (filtered) => {
  apps = JSON.parse(filtered[0]);
  themes = JSON.parse(filtered[1]);
  loadAll();
});