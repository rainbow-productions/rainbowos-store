const socket = io();
const cw = new Clockwork();

var loggedIn = null;
var username = null;
var token = null;

var apps = [];
var themes = [];
var myApps = [];
var myThemes = [];
var pages = {apps: 1, themes: 1};
var currentPage = {apps: 1, themes: 1}

if ( window === window.parent ) {
  alert("it seems you aren't viewing the app store on clockwork! keep in mind you wont be able to install apps")
  document.body.style = "background-color: #000;"
} 

function openTab(tab) {
  document.getElementById("curated").style = "display: none;";
  document.getElementById("apps").style = "display: none;";
  document.getElementById("themes").style = "display: none;";
  document.getElementById("dev").style = "display: none;";
  document.getElementById(tab).style = "";
}

//window.parent.postMessage(["install_app","installApp",script], "*");

function viewAppSource() {
  var app = myApps.find((x) => {
    return x.url == document.getElementById("remapp-select")
      [document.getElementById("remapp-select").selectedIndex].id;
  })
  if (app) {
    window.open(app.url);
  } else {
    alert("app doesnt exist")
  }
}

function loadAll() {
  // APPS
  if (document.getElementById("apps")) {
    document.getElementById("apps").remove()
  }
  if (document.getElementById("themes")) {
    document.getElementById("themes").remove()
  }
  var html = '<div class="grid-container">';
  for (var i = 0; i < apps.length; i++) {
    html += `<div class="grid-item">
    <div class="grid-objects">
      <span style="font-size: 17px;"><b>`+apps[i].name+`</b> by `+apps[i].uploader+`</span><br>
      `+apps[i].description+`
    </div><br>
    <div class="grid-objects">
      <img class="grid-objects" src="`+apps[i].icon+`">
      <input type="button" onclick="cw.installApp('`+apps[i].url+`');" value="Install" class="button1" style="height: 36px; vertical-align: top;">
    </div>
  </div>`;
  }
  html += `</div>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('apps', -20);">&lt;&lt;</a>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('apps', -1);">&lt;</a>
  
  <span id="pagenumapps" 
  type="number">
  ${currentPage.apps}</span> /
  ${pages.apps}
  
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('apps', 1);">&gt;</a>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('apps', 20);">&gt;&gt;</a>`
  var div = document.createElement("DIV");
  div.innerHTML = html;
  div.id = "apps";
  document.body.appendChild(div);
  document.getElementById("apps").innerHTML = html;
  
  // THEMES
  var html = "<div class=\"theme-container\">";
  for (var i = 0; i < themes.length; i++) {
    html += `<div class="grid-item banner-container">
    <div class="grid-objects">
      <span style="font-size: 17px;"><b>`+themes[i].name+`</b> by `+themes[i].uploader+`</span><br>
      `+(function() {
      if (themes[i].description) return themes[i].description;
      return "No description.";
    })()+` 
    </div><br>
    <div class="grid-objects"><img class="grid-objects banner" src="${themes[i].banner}" onerror="this.remove();">
      <input type="button" onclick="cw.installTheme('`+themes[i].url+`');" value="Install" class="button1" style="height: 36px; vertical-align: top;">
    </div>
  </div>`;
  }
  html += `</div>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('appsthemes', -20);">&lt;&lt;</a>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('themes', -1);">&lt;</a>
  
  <span id="pagenumapps" 
  type="number">
  ${currentPage.themes}</span> /
  ${pages.themes}
  
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('themes', 1);">&gt;</a>
  <a class="button" style="display: inline-block; width: 20px; text-align: center;"
  onclick="changePage('themes', 20);">&gt;&gt;</a>`
  var div = document.createElement("DIV");
  div.innerHTML = html;
  //div.className = "grid-container";
  div.id = "themes";
  document.body.appendChild(div);
  document.getElementById("themes").innerHTML = html;
  document.getElementById("themes").style = "display: none;";
  document.getElementById("loading").style = "display: none;";
}

function changePage(type, amount) {
  if (type == 'apps') {
    document.getElementById("pagenumapps").innerHTML =
    Number(document.getElementById("pagenumapps").innerHTML) + amount
    if (Number(document.getElementById("pagenumapps").innerHTML) > pages.apps) {
      document.getElementById("pagenumapps").innerHTML = pages.apps
    }
    if (Number(document.getElementById("pagenumapps").innerHTML) < 1) {
      document.getElementById("pagenumapps").innerHTML = 1
    }
    currentPage.apps = Number(document.getElementById("pagenumapps").innerHTML);
    loadit()
  }
}

// This part gets the list of apps!
function loadit() {
  socket.emit("get data", {
    page: currentPage,
  }, (data) => {
    apps = data.apps;
    themes = data.themes;
    pages = data.pages
    loadAll();
  });
}

function devUpdateApps() {
  document.getElementById("applimit-cur").innerHTML = myApps.length;
  var html = "";
  for (var i = 0; i < myApps.length; i++) {
    html += `<option id=${myApps[i].url}>${myApps[i].name} (${myApps[i].url.split("/")[2]})</option>`
  }
  document.getElementById("remapp-select").innerHTML = html;
}
function devUpdateThemes() {
  document.getElementById("themelimit-cur").innerHTML = myThemes.length;
  var html = "";
  for (var i = 0; i < myThemes.length; i++) {
    html += `<option id=${myThemes[i].url}>${myThemes[i].name} (${myThemes[i].url.split("/")[2]})</option>`
  }
  document.getElementById("remtheme-select").innerHTML = html;
}

function attemptAddApp() {
  socket.emit("add app", {
    url: document.getElementById("addapp-url").value,
  }, (data) => {
    document.getElementById("addapp-error").innerHTML = data.result;
    if (data.result == "error") {
      document.getElementById("addapp-error").innerHTML += data.error;
    }
    if (data.apps) {
      myApps = data.apps;
      devUpdateApps();
    }
  });
}

function attemptAddTheme() {
  socket.emit("add theme", {
    url: document.getElementById("addtheme-url").value,
  }, (data) => {
    document.getElementById("addtheme-error").innerHTML = data.result;
    if (data.result == "error") {
      document.getElementById("addtheme-error").innerHTML += data.error;
    }
    if (data.themes) {
      myApps = data.themes;
      //devUpdateThemes();
    }
  });
}
    
function adminCommand() {
  socket.emit("admin cmd", {
    text: document.getElementById("cmdtext").value,
  }, (data) => {
    alert(data.result);
  });
}

function attemptRemoveApp() {
  if (prompt("Are you sure you want to delete "+document.getElementById("remapp-select")
    [document.getElementById("remapp-select").selectedIndex].value
    +"? Type \"delete\" to confirm.")
     == "delete") {
    socket.emit("remove app", {
      url: document.getElementById("remapp-select")
      [document.getElementById("remapp-select").selectedIndex].id,
    }, (data) => {
      document.getElementById("remapp-error").innerHTML = data.result;
      if (data.result == "error") {
        document.getElementById("remapp-error").innerHTML += data.error;
      }
      if (data.apps) {
        myApps = data.apps;
        devUpdateApps();
      }
    });
  };
}

function attemptRemoveTheme() {
  if (prompt("Are you sure you want to delete "+document.getElementById("remtheme-select")
    [document.getElementById("remtheme-select").selectedIndex].value
    +"? Type \"delete\" to confirm.")
     == "delete") {
    socket.emit("remove theme", {
      url: document.getElementById("remtheme-select")
      [document.getElementById("remtheme-select").selectedIndex].id,
    }, (data) => {
      document.getElementById("remtheme-error").innerHTML = data.result;
      if (data.result == "error") {
        document.getElementById("remtheme-error").innerHTML += data.error;
      }
      if (data.themes) {
        myThemes = data.themes;
        devUpdateThemes();
      }
    });
  };
}

function attemptEditApp() {
  socket.emit("edit app", {
      url: document.getElementById("remapp-select")
      [document.getElementById("remapp-select").selectedIndex].id,
    }, (data) => {
      document.getElementById("editapp-error").innerHTML = data.result;
      if (data.result == "error") {
        document.getElementById("editapp-error").innerHTML += data.error;
      }
      if (data.apps) {
        myApps = data.apps;
        devUpdateApps();
      }
    });
}
function attemptEditTheme() {
  socket.emit("edit theme", {
      url: document.getElementById("remtheme-select")
      [document.getElementById("remtheme-select").selectedIndex].id,
    }, (data) => {
      document.getElementById("edittheme-error").innerHTML = data.result;
      if (data.result == "error") {
        document.getElementById("edittheme-error").innerHTML += data.error;
      }
      if (data.themes) {
        myThemes = data.themes;
        devUpdateThemes();
      }
    });
}

function attemptLogin() {
  socket.emit("login", {
    username: document.getElementById("login-un").value,
    password: document.getElementById("login-pw").value
  }, (data) => {
    document.getElementById("login-error").innerHTML = data.result;
    if (data.token) {
      token = data.token;
      console.log(data.apps);
      console.log(data.apps.length);
      username = document.getElementById("login-un").value;
      document.getElementById("login-stuff").style = "display: none;";
      document.getElementById("dev-stuff").style = "";
      document.getElementById("welcome").style = "";
      document.getElementById("welcome").innerHTML = `Welcome, ${username}.`;
      if (data.admin) {
        document.getElementById("adminpanel").style = "";
      }
      myApps = data.apps;
      myThemes = data.themes;
      devUpdateApps();
      devUpdateThemes();
    } 
  });
}

function attemptSignUp() {
  socket.emit("sign up", {
    username: document.getElementById("signup-un").value,
    password: document.getElementById("signup-pw").value,
    captcha: document.getElementById("signup-cap").value
  }, (data) => {
    document.getElementById("signup-error").innerHTML = data.result;
    if (data.token) {
      token = data.token;
      console.log(data.apps);
      console.log(data.apps.length);
      username = document.getElementById("signup-un").value;
      document.getElementById("login-stuff").style = "display: none;";
      document.getElementById("dev-stuff").style = "";
      document.getElementById("dev-stuff").style = "";
      document.getElementById("welcome").style = "";
      document.getElementById("welcome").innerHTML = `Welcome, ${username}.`;
      myApps = data.apps;
      devUpdateApps();
    } 
  });
}

function getCaptcha() {
  socket.emit("get captcha", {
    page: currentPage,
  }, (data) => {
    document.getElementById("captcha").src = data.captcha
  });
}

getCaptcha();
loadit();